<?php

//categores
Route::get('/user-category-view','CategoryController@user_category_view');

//products
Route::get('/user-best-new-product','productController@user_product_view');

//Shop 
Route::get('/user-shop-products-all','ShopController@user_shop_products_all');
Route::get('/user-shop-products-by-category/{id}','ShopController@user_shop_products_by_category');
Route::get('/sort-low-to-high/{id}','ShopController@sorted_by_low_to_high');
Route::get('/sort-high-to-low/{id}','ShopController@sorted_by_high_to_low');
Route::post('/price-slider','ShopController@price_slider');

//Cart 
Route::post('/add-to-cart','CartController@add_to_cart');
Route::get('/cart-info','CartController@cart_products');
Route::post('/delete-from-cart','CartController@remove_from_cart');
Route::post('/add-to-cart-qty','CartController@add_to_cart_with_qty');
Route::post('/add-to-cart-qty-modal','CartController@add_to_cart_with_qty_modal');




//Authentication
Route::post('/login','AuthController@login')->name('login');
Route::post('/register-user','AuthController@register')->name('register');
Route::get('/user-authenticate','CheckoutController@is_authentic');

//Search route
Route::get('/search','ShopController@findSearchProducts');
Route::post('/sort-low-to-high-search','SearchController@sorted_by_low_to_high_search');
Route::post('/sort-high-to-low-search','SearchController@sorted_by_high_to_low_search');
Route::post('/price-slider-search','SearchController@price_slider_search');





//myaccount
Route::get('/getuser','MyaccountController@get_authentic_user');




/* front users vue js purpose */

//first setup base route without content

Route::get('/', function () {
    return view('users.app');
});

Route::get('/shop', function () {
    return view('users.app');
});

Route::get('/cart', function () {
    return view('users.app');
});

Route::get('/checkout', function () {
    return view('users.app');
});

Route::get('/searchresults', function () {
    return view('users.app');
});

Route::get('/myaccount', function () {
    return view('users.app');
});

Route::get('/login', function () {
    return view('users.app');
})->name('login');

Route::get('/register', function () {
    return view('users.app');
})->name('register');

/* front users vue js purpose end */

