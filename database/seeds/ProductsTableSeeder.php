<?php

use Illuminate\Database\Seeder;
use App\Product;
use Carbon\Carbon;

class ProductsTableSeeder extends Seeder
{
   
    public function run()
    {
    	$now = Carbon::now()->toDateTimeString();

        Product::insert([

        	[
        	'category_id'=>1,'image'=>'default.png','images' =>'default.png','name'=>'T-shirt1','slug'=>'t-shirt','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>1,'status'=>1,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>1,'image'=>'default.png','images' =>'default.png','name'=>'Pants','slug'=>'pants','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>1000,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>1,'status'=>1,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

            [
            'category_id'=>1,'image'=>'default.png','images' =>'default.png','name'=>'Men Shirt','slug'=>'men-shirt','details'=>'Lorem ipsum dolor sit amet, consectetur',
            'price'=>500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
            'featured'=>2,'status'=>2,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
            ],

            [
            'category_id'=>1,'image'=>'default.png','images' =>'default.png','name'=>'Men Three Piece','slug'=>'men-three-piece','details'=>'Lorem ipsum dolor sit amet, consectetur',
            'price'=>1000,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
            'featured'=>3,'status'=>3,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
            ],

            [
            'category_id'=>1,'image'=>'default.png','images' =>'default.png','name'=>'Men 3 Quarter','slug'=>'men-3-quarter','details'=>'Lorem ipsum dolor sit amet, consectetur',
            'price'=>500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
            'featured'=>1,'status'=>1,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
            ],

            [
            'category_id'=>1,'image'=>'default.png','images' =>'default.png','name'=>'Men Genji','slug'=>'men-genji','details'=>'Lorem ipsum dolor sit amet, consectetur',
            'price'=>1000,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
            'featured'=>3,'status'=>3,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
            ],

        	[
        	'category_id'=>2,'image'=>'default.png','images' =>'default.png','name'=>'Saree','slug'=>'saree','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>2500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>2,'status'=>2,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>2,'image'=>'default.png','images' =>'default.png','name'=>'Jama','slug'=>'jama','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>1500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>2,'status'=>2,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

            [
            'category_id'=>2,'image'=>'default.png','images' =>'default.png','name'=>'Women Tops','slug'=>'women-tops','details'=>'Lorem ipsum dolor sit amet, consectetur',
            'price'=>2500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
            'featured'=>3,'status'=>3,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
            ],

            [
            'category_id'=>2,'image'=>'default.png','images' =>'default.png','name'=>'Women 3 Piece','slug'=>'women-3-piece','details'=>'Lorem ipsum dolor sit amet, consectetur',
            'price'=>1500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
            'featured'=>2,'status'=>2,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
            ],

            [
            'category_id'=>2,'image'=>'default.png','images' =>'default.png','name'=>'Plajo','slug'=>'plajo','details'=>'Lorem ipsum dolor sit amet, consectetur',
            'price'=>2500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
            'featured'=>1,'status'=>1,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
            ],

            [
            'category_id'=>2,'image'=>'default.png','images' =>'default.png','name'=>'Orna','slug'=>'orna','details'=>'Lorem ipsum dolor sit amet, consectetur',
            'price'=>1500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
            'featured'=>1,'status'=>1,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
            ],

        	[
        	'category_id'=>3,'image'=>'default.png','images' =>'default.png','name'=>'Baby Shirt','slug'=>'baby-shirt','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>400,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>3,'status'=>3,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>3,'image'=>'default.png','images' =>'default.png','name'=>'Baby Pant','slug'=>'baby-pant','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>3,'status'=>3,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

            [
            'category_id'=>3,'image'=>'default.png','images' =>'default.png','name'=>'Baby Panjabi','slug'=>'baby-panjabi','details'=>'Lorem ipsum dolor sit amet, consectetur',
            'price'=>400,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
            'featured'=>2,'status'=>2,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
            ],

            [
            'category_id'=>3,'image'=>'default.png','images' =>'default.png','name'=>'Baby Genji','slug'=>'baby-genji','details'=>'Lorem ipsum dolor sit amet, consectetur',
            'price'=>500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
            'featured'=>1,'status'=>1,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
            ],

            [
            'category_id'=>3,'image'=>'default.png','images' =>'default.png','name'=>'Baby Cap','slug'=>'baby-cap','details'=>'Lorem ipsum dolor sit amet, consectetur',
            'price'=>400,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
            'featured'=>2,'status'=>2,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
            ],

            [
            'category_id'=>3,'image'=>'default.png','images' =>'default.png','name'=>'Baby Jinse','slug'=>'baby-jinse','details'=>'Lorem ipsum dolor sit amet, consectetur',
            'price'=>500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
            'featured'=>3,'status'=>3,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
            ],

        	[
        	'category_id'=>4,'image'=>'default.png','images' =>'default.png','name'=>'Apple Watch','slug'=>'apple-watch','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>5000,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>1,'status'=>1,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>4,'image'=>'default.png','images' =>'default.png','name'=>'Google Watch','slug'=>'google-watch','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>4500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>1,'status'=>1,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>5,'image'=>'default.png','images' =>'default.png','name'=>'Italian Boss','slug'=>'italian-boss','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>1000,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>2,'status'=>2,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>5,'image'=>'default.png','images' =>'default.png','name'=>'Carrera','slug'=>'carrera','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>1500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>3,'status'=>3,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>6,'image'=>'default.png','images' =>'default.png','name'=>'Jwellery Chain','slug'=>'jwellery-chain','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>100500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>3,'status'=>3,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>6,'image'=>'default.png','images' =>'default.png','name'=>'Face Wash','slug'=>'face-wash','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>2,'status'=>2,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>7,'image'=>'default.png','images' =>'default.png','name'=>'Samsung Smart','slug'=>'samsung-smart','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>88500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>1,'status'=>1,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>7,'image'=>'default.png','images' =>'default.png','name'=>'Lg Butterfly','slug'=>'lg-butterfly','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>70500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>1,'status'=>1,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>8,'image'=>'default.png','images' =>'default.png','name'=>'Huawei','slug'=>'huawei','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>2500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>3,'status'=>3,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>8,'image'=>'default.png','images' =>'default.png','name'=>'Oppo Gen5','slug'=>'oppo-gen5','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>55500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>3,'status'=>3,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>9,'image'=>'default.png','images' =>'default.png','name'=>'Hp First','slug'=>'hp-first','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>50500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>2,'status'=>2,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>9,'image'=>'default.png','images' =>'default.png','name'=>'Apple 6 node','slug'=>'apple-6-node','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>67500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>2,'status'=>2,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>10,'image'=>'default.png','images' =>'default.png','name'=>'Samsung Extreme','slug'=>'samsung-extreme','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>75500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>2,'status'=>2,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        	[
        	'category_id'=>10,'image'=>'default.png','images' =>'default.png','name'=>'Lenovo Pro','slug'=>'lenovo-pro','details'=>'Lorem ipsum dolor sit amet, consectetur',
        	'price'=>65500,'description'=>'adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus',
        	'featured'=>2,'status'=>2,'quantity'=>5,'created_at'=>$now,'updated_at'=>$now 
        	],

        ]);
        
    }
}
