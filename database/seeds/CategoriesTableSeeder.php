<?php

use Illuminate\Database\Seeder;
use App\Category;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
   
    public function run()
    {
    	$now = Carbon::now()->toDateTimeString();

        Category::insert([

        	['name'=>'Men Clothing','slug' => 'men-clothing' ,'created_at'=>$now, 'updated_at' =>$now ],
        	['name'=>'Women Clothing','slug' => 'Women-clothing' ,'created_at'=>$now, 'updated_at' =>$now ],
        	['name'=>'Baby Clothing','slug' => 'baby-clothing' ,'created_at'=>$now, 'updated_at' =>$now ],
        	['name'=>'Watch','slug' => 'Watch' ,'created_at'=>$now, 'updated_at' =>$now ],
        	['name'=>'Glasses','slug' => 'Glasses' ,'created_at'=>$now, 'updated_at' =>$now ],
        	['name'=>'Gifts','slug' => 'Gifts' ,'created_at'=>$now, 'updated_at' =>$now ],
        	['name'=>'Electronics','slug' => 'electronics' ,'created_at'=>$now, 'updated_at' =>$now ],
        	['name'=>'Mobiles','slug' => 'mobiles' ,'created_at'=>$now, 'updated_at' =>$now ],
        	['name'=>'Laptops','slug' => 'laptops' ,'created_at'=>$now, 'updated_at' =>$now ],
        	['name'=>'Desktops','slug' => 'desktops' ,'created_at'=>$now, 'updated_at' =>$now ],

        ]);
    }

}
