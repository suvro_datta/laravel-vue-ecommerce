<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{

    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->text('image')->nullable();
            $table->binary('images')->nullable();
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->string('details')->nullable();
            $table->integer('price');
            $table->text('description');
            $table->boolean('featured')->default(false);
            $table->boolean('status')->default(false);
            $table->unsignedInteger('quantity')->default(1);

            $table->foreign('category_id')->references('id')
                  ->on('categories')->onDelete('cascade');   
            $table->timestamps();

        });
    }

   
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
