<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{

    public function is_authentic(){

    	$is_authenticated = 0;
    	if( Auth::check() ){
           $is_authenticated = 1;
    	}

    	$msg = [

    			'status' => 'success',
    			'message' => 'authentication is checking now',
    			'is_authenticated' => $is_authenticated
    	];

    	return response()->json($msg);

    }

}
