<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    public function user_product_view(){

    	/*get latest 4 categories*/
    	$categories = Category::with('products')->latest()->take(4)->get();

       // return $categories[0]->products;

    	$best = Product::where('status','=',1)->take(50)->get();
    	$new =  Product::latest()->take(50)->get();

    	$all_data = [

    		'status'  => 'successful',
    		'message' => 'products found successful',
    		'best_seller' => $best,
    		'new_arrival' => $new,
    		'four_categories' => $categories,

    	];

    	return response()->json($all_data);

    }
}
