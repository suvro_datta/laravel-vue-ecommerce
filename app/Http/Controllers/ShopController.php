<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ShopController extends Controller
{
    public function user_shop_products_all(){

    	$all_products = Product::take(150)->get();

    	$all_data = [

    		'status'  		=> 'successful',
    		'message' 		=> 'products found successful',
    		'all_products'  => $all_products,

    	];

    	return response()->json($all_data);
    }

    public function user_shop_products_by_category( $id ){

    	$products_by_category = Category::with('products')->where('id','=',$id)->get();

    	$all_data = [

    		'status'  		=> 'successful',
    		'message' 		=> 'products found by category successful',
    		'all_category_products_using_id'  => $products_by_category,

    	];

    	return response()->json($all_data);

    }

    public function sorted_by_low_to_high($id){

        if( $id != -1 ){
            $products_by_low_to_high = Product::select()->where('category_id',$id)->orderBY('price','asc')->get();
        }else{

        $products_by_low_to_high = Product::select()->orderBY('price','asc')->get();

        }

        //$products_by_high_to_low = Product::select('price')->orderBY('price','desc')->get();

        $all_data = [

            'status'        => 'successful',
            'message'       => 'products found successful',
            'all_products'  => $products_by_low_to_high,

        ];

        return response()->json($all_data);
        
    }

    public function sorted_by_high_to_low($id){

        if( $id != -1 ){
            $products_by_low_to_high = Product::select()->where('category_id',$id)->orderBY('price','desc')->get();
        }else{

        $products_by_low_to_high = Product::select()->orderBY('price','desc')->get();

        }

        //$products_by_high_to_low = Product::select('price')->orderBY('price','desc')->get();

        $all_data = [

            'status'        => 'successful',
            'message'       => 'products found successful',
            'all_products'  => $products_by_low_to_high,

        ];

        return response()->json($all_data);

    }

    public function price_slider(Request $request){

        $id  = $request->id;
        $min = $request->min;
        $max = $request->max;

        if( $id != -1 ){
            $products = Product::select()->where('category_id',$id)
                                ->where('price','<=',$max)->where('price','>=',$min)
                                ->get();
        }else{
            $products = Product::select()->where('price','<=',$max)->where('price','>=',$min)->get();
        }


        $all_data = [

            'status'        => 'successful',
            'message'       => 'products found successful',
            'all_products'  => $products,

        ];

        return response()->json($all_data);

    }


    public function findSearchProducts(){

        $search = \Request::get('s');

        session()->put('search_data',$search);

        if( $search != NULL ){

            $products = Product::where('name','LIKE',"%$search%")
                         ->orWhere('description','LIKE',"%$search%")
                         ->get();
            $message = [
                "status" => "success",
                "message" => "found single product",
                "all_products"  => $products,
            ];

            return response()->json($message);

        }else{
            return $this->user_shop_products_all();
        }
    }

    public function sorted_by_low_to_high_insearch(){

        session::has('search_data');

        if( $id != -1 ){
            $products_by_low_to_high = Product::select()->where('category_id',$id)->orderBY('price','asc')->get();
        }else{

        $products_by_low_to_high = Product::select()->orderBY('price','asc')->get();

        }

        //$products_by_high_to_low = Product::select('price')->orderBY('price','desc')->get();

        $all_data = [

            'status'        => 'successful',
            'message'       => 'products found successful',
            'all_products'  => $products_by_low_to_high,

        ];

        return response()->json($all_data);
        
    }

}
