<?php

namespace App\Http\Controllers;
use App\Category;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function user_category_view(){

    	$categories = Category::all();

    	$all_data = [

    		'status' => 'successful',
    		'message'  => 'successfully get all categories',
    		'categories' => $categories

    	];

    	return response()->json($all_data);

    }
}
