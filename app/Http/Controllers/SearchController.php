<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class SearchController extends Controller
{
    public function sorted_by_low_to_high_search(Request $request){

    	$search = $request->key;

    	if( $search != "-1" ){
    	    $products_by_low_to_high = Product::select()
	    									->where('name','LIKE',"%$search%")
	    									->orWhere('description','LIKE',"%$search%")
	    									->orderBY('price','asc')
	    									->get();
    	}else{

    	$products_by_low_to_high = Product::select()->orderBY('price','asc')->get();

    	}

    	//$products_by_high_to_low = Product::select('price')->orderBY('price','desc')->get();

    	$all_data = [

    	    'status'        => 'successful',
    	    'message'       => 'products found successful',
    	    'all_products'  => $products_by_low_to_high,

    	];

    	return response()->json($all_data);

    }

        public function sorted_by_high_to_low_search(Request $request){

        	$search = $request->key;

        	if( $search != "-1" ){
        	    $products_by_high_to_low = Product::select()
    	    									->where('name','LIKE',"%$search%")
    	    									->orWhere('description','LIKE',"%$search%")
    	    									->orderBY('price','desc')
    	    									->get();
        	}else{

        	$products_by_high_to_low = Product::select()->orderBY('price','desc')->get();

        	}

        	//$products_by_high_to_low = Product::select('price')->orderBY('price','desc')->get();

        	$all_data = [

        	    'status'        => 'successful',
        	    'message'       => 'products found successful',
        	    'all_products'  => $products_by_high_to_low,

        	];

        	return response()->json($all_data);

        }

        public function price_slider_search(Request $request){

            $min = $request->min;
            $max = $request->max;
            $search = $request->key;

            if( $search != "-1" ){
                $products = Product::select()
                					->where('name','LIKE',"%$search%")
    	    						//->orWhere('description','LIKE',"%$search%")
                                    ->where('price','<=',$max)->where('price','>=',$min)
                                    ->get();
            }else{
                $products = Product::select()->where('price','<=',$max)->where('price','>=',$min)->get();
            }


            $all_data = [

                'status'        => 'successful',
                'message'       => 'products found successful',
                'all_products'  => $products,

            ];

            return response()->json($all_data);

        }

}
