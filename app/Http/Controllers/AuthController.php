<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use Auth;

class AuthController extends Controller
{

    public function login(Request $request){

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (Auth::attempt($credentials)) {
            
            // $token = auth()->user()->createToken('AppName')->accessToken;
            // $success['token'] =  $token;
            // $success['name'] =  auth()->user()->name;
            // $success['id'] =  auth()->user()->id;
          $user = Auth::user();
          $success['token'] = $user->createToken('AppName')->accessToken;
          $success['user'] = $user;
          return response()->json(['success'=>$success], 200);

        } else {
            return response()->json(['error' => 'UnAuthorised'], 401);
        }

        // $data = 0;

        // $user= User::select('password')->where('email',$request->email)
                             
        //                      ->first();

        // if( Hash::check($request->password, $user->password ) ){
        //     $data = 1;

        // }

        // // session()->put('login_user','loggedin');

        // $msg = [

        //     'status' => 'success',
        //     'message' => 'successfully added register',
        //     //'password' => $request->password,
        //     'grant_type' => 'password',
        //     'client_id' => config('services.passport.client_id'),
        //     'client_secret' => config('services.passport.client_secret'),
        //     'endpoint' => config('services.passport.login_endpoint'),
        //     'guzzle' => $http = new \GuzzleHttp\Client(),
        //     'email' => $request->email,
        //     'password' => $request->password, 

        // ];

        // return response()->json($msg);


        


    	 // $http = new \GuzzleHttp\Client();

      //    $data = $http->request('POST', config('services.passport.login_endpoint'), [
      //   'form_params' => [
      //       'grant_type' => 'password',
      //       'client_id' => config('services.passport.client_id'),
      //       'client_secret' => config('services.passport.client_secret'),
      //       'email' => $request->email,
      //       'password' => $request->password,
      //   ]
      //   ]);

      //    return response()->json($data);

      //   try {

      //       $response = $http->post(config('services.passport.login_endpoint'), [
      //           'form_params' => [
      //               'grant_type' => 'password',
      //               'client_id' => config('services.passport.client_id'),
      //               'client_secret' => config('services.passport.client_secret'),
      //               'email' => $request->email,
      //               'password' => $request->password,
      //           ]
      //       ]);

      //       return $response->getBody();

      //   } 
        //catch (\GuzzleHttp\Exception\BadResponseException $e) {

     //        if ($e->getCode() === 400) {
     //            return response()->json('Invalid Request. Please enter a username or a password.', $e->getCode());
     //        } else if ($e->getCode() === 401) {
     //            return response()->json('Your credentials are incorrect. Please try again', $e->getCode());
     //        }

     //        return response()->json('Something went wrong on the server.', $e->getCode());
     //    }

     //    return response()->json($msg);

    }

    public function register(Request $request){

    	$request->validate([
            'username' => 'required|string|max:255',
            'phone' => 'required|string|max:15',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = new User;

        $user->username = $request->username;
        $user->email    = $request->email;
        $user->phone    = $request->phone;
        $user->password = Hash::make($request->password);
        $user->save();
        

        $msg = [

            'status' => 'success',
            'message' => 'successfully added register',
            'info' => $request->all() 

        ];

        return response()->json($msg);

    }

}
