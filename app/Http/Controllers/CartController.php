<?php

namespace App\Http\Controllers;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{

    public function add_to_cart_with_qty_modal(Request $request){

        Cart::add($request->id, $request->name, $request->quantity, $request->price)->associate('App\Product');

        $msg = [

            'status' => 'success',
            'message' => 'item added to cart',
            'request' => $request->all(),

        ];

        return response()->json($msg);


    }

    
    public function add_to_cart(Request $request){

    	Cart::add($request->id, $request->name, 1, $request->price)->associate('App\Product');

    	$msg = [

    		'status' => 'success',
    		'message' => 'item added to cart',
    		'request' => $request->all(),

    	];

    	return response()->json($msg);


    }

    public function add_to_cart_with_qty(Request $request){

        $qty = $request->quantity;

        if( $qty <= 0 ){

            Cart::remove($request->main_id);

        }else if( $qty>=1 ){

            Cart::update($request->main_id, $request->quantity);

            //Cart::remove($request->main_id);

           // Cart::add($request->id, $request->name, $request->quantity, $request->price)->associate('App\Product');

        }

        $msg = [

            'status' => 'success',
            'message' => 'item added to cart',
            'request' => $qty+10,

        ];

        return response()->json($msg);


    }

    public function cart_products(){

    	$total_item = Cart::count();
    	$cart_products = Cart::content();
    	$total_tk = Cart::total();
    	$cart_tax = Cart::tax();
    	$cart_subtotal_tk = Cart::subtotal();

    	$msg = [

    		'status' => 'success',
    		'message' => 'cart info founds',
    		'total_item' => $total_item,
    		'cart_products' => $cart_products,
    		'cart_total' => $total_tk,
    		'cart_tax' => $cart_tax,
    		'cart_subtotal' => $cart_subtotal_tk,

    	];

    	return response()->json($msg);

    }

    public function remove_from_cart(Request $request){

    	Cart::remove($request->id);

    	$msg = [

    		'status' => 'success',
    		'message'=> 'cart item removed',
    		//'id'     => $request->id,

    	];

    	return response()->json($msg);

    }

}
