//require('./bootstrap');

window.Vue = require('vue');
window.axios = require('axios');
//vue router staff
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import {routes} from './routes';

const router = new VueRouter({
   routes, // short for `routes: routes`
   mode:'history',
    scrollBehavior() {
        return {x: 0, y: 0};
    }
});

//vuex staff
import Vuex from 'vuex';
Vue.use(Vuex);
import storeFrontData from './store/front.js';
const store = new Vuex.Store(
	storeFrontData
);

router.beforeEach((to,from,next)=>{

  //console.log(to.matched.some(record));
  // next();
  //console.log(to.meta);

  document.title = to.meta.title;

  if( to.meta.requiresAuth ){

    console.log(to.meta.requiresAuth);

      if( !store.getters.loggedIn ){
        next({name:'login'});
      }else{
        next();
      }
  }

  else if( to.meta.requiresVisitor ){

    console.log(to.meta.requiresVisitor);

      if( store.getters.loggedIn ){
        next({name:'checkout'});
      }else{
        next();
      }
  }
  else{
    next()
  }

});


Vue.component('home-comp', require('./components/front/Home.vue'));
Vue.component('shop-comp', require('./components/front/Shop.vue'));
Vue.component('shopcart', require('./components/front/Cartheader.vue'));
Vue.component('master-comp', require('./components/front/Master.vue'));
Vue.component('search', require('./components/front/Search.vue'));

const app = new Vue({
    el: '#app',
    //support vue router staff here
    router,
    store,

});
