import HomeComponent from './components/front/Home.vue';
import ShopComponent from './components/front/Shop.vue';
import CartComponent from './components/front/Cart.vue';
import CheckoutComponent from './components/front/Checkout.vue';
import LoginComponent from './components/authentic/Login.vue';
import RegisterComponent from './components/authentic/Register.vue';
import Search from './components/front/Searchresults.vue';
import Myaccount from './components/front/Myaccount.vue';

//import store from './store/front.js';


export const routes = [
	
	{
		path:'/',
		component:HomeComponent,
		meta:{title: 'Home'}
		

	},
	{
		path:'/shop',
		component:ShopComponent,
		meta:{title: 'Shop'}
		
	},
	{
		path:'/categories/:id',
		component:ShopComponent,
		meta:{title: 'Shop'}
				
	},
	{
		path:'/cart',
		component:CartComponent,
		meta:{title: 'Cart'}
				
	},
	{
		path:'/checkout',
		component:CheckoutComponent,
		name: 'checkout',
		meta:{title:'Checkout',requiresAuth:true},
				
	},
	{
		path:'/login',
		component:LoginComponent,
		name: 'login',
		meta:{title:'Login',requiresVisitor:true}
				
	},
	{
		path:'/register',
		component:RegisterComponent,
		meta:{title:'Register',requiresVisitor:true}
				
	},
	{
		path:'/searchresults',
		component:Search,
		meta:{title:'Search'}
				
	},
	{
		path:'/myaccount',
		component:Myaccount,
		meta:{title:'Myaccount',requiresAuth:true}
				
	}

];