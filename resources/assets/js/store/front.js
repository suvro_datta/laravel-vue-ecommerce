export default{

	state:{

		token: localStorage.getItem('access_token') || null,

		//all categories
		all_categories:[],
		//best new products for home
		all_best_new_products:[],

		shop_products_all:[],

		cart_products:[],

		keyword:"",

	},
	getters:{

		loggedIn(state) {
	      return state.token !== null;
	    },

		//all categories
		get_all_categories( state ){
			return state.all_categories;
		},

		//best new products for home
		get_all_best_new_products(state){
			return state.all_best_new_products;
		},

		get_shop_products_all(state){
			return state.shop_products_all;
		},

		get_cart_info(state){
			return state.cart_products;
		},

		get_keyword(state){
			//console.log(state.keyword);
			return state.keyword;
		}

	},
	actions:{

			setKeyword(context,payload){

				//console.log(payload);
				context.state.keyword = payload;
				//context.commit('mute_keyword',payload);

			},

			//for search
			searchPosts(context,payload){
				axios.get('/search?s='+payload)
			        .then((response)=>{
			        	console.log("its search data");
			            console.log(response.data.all_products);
			            context.commit('mute_searchdata',response.data.all_products);
			    })

			},

			action_price_slider(context,payload){
				axios.post('/price-slider',{id:payload.id,min:payload.min,max:payload.max}).then((response)=>{

					console.log( response.data.all_products );
					context.commit("mute_all_slider_product",response.data.all_products);

				});
			},

			action_register(context,payload){

				return new Promise((resolve, reject) => {

					axios.post('/register-user', 
						{ password:payload.password,email:payload.email,username:payload.username,phone:payload.phone })
					.then(function (response) {

					   resolve(response);
					   console.log(response.data.info);			   

					})
					.catch((error)=>{
						reject(error);
					})

				});

			},

			action_remove_token(context){
				if( context.getters.loggedIn ){
					localStorage.removeItem('access_token');
					context.commit('mute_delete_token');
				}
			},

			retrieveToken(context, credentials) {

		      return new Promise((resolve, reject) => {
		        axios.post('/login', {
		          email: credentials.email,
		          password: credentials.password,
		        })
		          .then(response => {
		          	
		            const token = response.data.success.token
		            console.log(token);
		            localStorage.setItem('access_token', token);
		            context.commit('mute_retrieveToken', token);
		            resolve(response);
		            // console.log(response);
		            // context.commit('addTodo', response.data)
		          })
		          .catch(error => {
		            console.log(error)
		            reject(error)
		          })
		        })
		    },

			//get cart infos
			action_cart_info(context){

				axios.get('/cart-info').then((response)=>{

					console.log( response.data.total_item );
					console.log( response.data.cart_products );
					context.commit("mute_all_cart_product",response.data);

				});

			},

			//all categories
			action_all_categories(context){

				axios.get('/user-category-view').then((response)=>{
					//console.log( response.data.categories );
					context.commit("mute_all_categories",response.data.categories);
				});

			},

			//home products 
			action_home_best_new_products(context){
				axios.get('/user-best-new-product').then((response)=>{
					//console.log(response.data);
					context.commit("mute_all_best_new_products",response.data);
				})
			},

			//shop products 
			action_shop_products_all(context){
				axios.get('/user-shop-products-all').then((response)=>{
					//console.log(response.data.all_products);
					context.commit("mute_shop_products_all",response.data.all_products);
				})
			},

			action_low_to_high(context,id){
				axios.get('/sort-low-to-high/'+id).then((response)=>{
					//console.log(response.data.all_products);
					context.commit("mute_low_to_high",response.data.all_products);
				})
			},

			action_high_to_low(context,id){
				axios.get('/sort-high-to-low/'+id).then((response)=>{
					//console.log(response.data.all_products);
					context.commit("mute_high_to_low",response.data.all_products);
				})
			},

			action_all_category_products(context,id){
				axios.get('/user-shop-products-by-category/'+id).then((response)=>{
					console.log(response.data.all_category_products_using_id[0].products);
					context.commit("mute_shop_products_by_id",response.data.all_category_products_using_id[0].products);
				})
			},

			action_low_to_high_search(context,keyword){

				console.log('inside axios' + keyword);
				axios.post('/sort-low-to-high-search',{key:keyword}).then((response)=>{
					console.log(response.data);
					context.commit("mute_low_to_high_search",response.data.all_products);

				})
			},

			action_high_to_low_search(context,keyword){

				console.log('inside axios' + keyword);
				axios.post('/sort-high-to-low-search',{key:keyword}).then((response)=>{
					console.log(response.data);
					context.commit("mute_high_to_low_search",response.data.all_products);

				})
			},

			action_price_slider_search(context,payload){
				axios.post('/price-slider-search',{key:payload.keyword,min:payload.min,max:payload.max}).then((response)=>{

					console.log( response.data.all_products );
					context.commit("mute_all_slider_product_search",response.data.all_products);

				});
			},

	},
	mutations:{

		mute_keyword(state,payload){
			this.keyword = payload;
		},

		//update posts on search data posts
		mute_searchdata(state,payload){
			state.shop_products_all = payload
		},

		mute_retrieveToken(state, token) {
	      state.token = token;
	    },

	    mute_delete_token(state){
	    	state.token = null;
	    },

		mute_all_cart_product(state,payload){
			return state.cart_products = payload;
		},

		mute_all_categories(state,payload){
			return state.all_categories = payload;
		},

		mute_all_best_new_products(state,payload){
			return state.all_best_new_products = payload;
		},

		mute_shop_products_all(state,payload){
			return state.shop_products_all = payload;
		},

		mute_shop_products_by_id(state,payload){
			return state.shop_products_all = payload;
		},

		mute_low_to_high(state,payload){
			return state.shop_products_all = payload;
		},

		mute_high_to_low(state,payload){
			return state.shop_products_all = payload;
		},

		mute_all_slider_product(state,payload){
			return state.shop_products_all = payload;
		},

		mute_low_to_high_search(state,payload){
			return state.shop_products_all = payload;
		},

		mute_high_to_low_search(state,payload){
			return state.shop_products_all = payload;
		},

		mute_all_slider_product_search(state,payload){
			return state.shop_products_all = payload;
		}

	}

}