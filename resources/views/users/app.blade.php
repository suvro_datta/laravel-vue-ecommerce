
<!doctype html>
<html class="no-js" lang="en">
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Home</title>
        <meta name="description" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="images/icons/icon_logo.png') }}">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="{{ asset('users/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('users/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('users/css/ionicons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('users/css/css-plugins-call.css') }}">
        <link rel="stylesheet" href="{{ asset('users/css/bundle.css') }}">
        <link rel="stylesheet" href="{{ asset('users/css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('users/css/responsive.css') }}">
        <link rel="stylesheet" href="{{ asset('users/css/colors.css') }}">

        <link rel="stylesheet" type="text/css" href="css/app.css">
       
        
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <!-- Body main wrapper start -->
        <div class="wrapper home-one" id="app">
            <!-- HEADER AREA START -->
            <header class="header-area">
               
                <!-- Header middle area start -->
                <div class="header-middle-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-2 col-md-12">
                                <!-- site-logo -->
                                <div class="site-logo">
                                    <router-link to="/">
                                    <img height="60" width="60" src="{{ asset('users/images/logo/shop_image.png') }}" alt="No Logo">
                                    </router-link>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-12">
                                <search><search/>
                            </div>
                            <div class="col-xl-4 col-md-12">

                                <!-- Shopping Cart Header -->
                                <shopcart></shopcart>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Header middle area end -->
                <!-- Header bottom area start -->
                <div class="header-bottom-area">
                    <div class="container">
                        <div class="row">
                           
                            <div class="col-lg-9">
                                <!-- main-menu -->
                                <div class="main-menu">
                                    <nav>
                                        <ul>
                                            <li class="current"><router-link to="/">Home</router-link></li>
                                            <li><router-link to="/shop">Shop</router-link></li>                                           
                                        </ul>
                                    </nav>
                                </div>
                                <div class="mobile-menu-area">
                                    <div class="mobile-menu">
                                        <nav id="mobile-menu-active">
                                            <ul class="menu-overflow">
                                                <li><router-link to="/">HOME</router-link></li>
                                                <li><router-link to="/shop">Shop</router-link></li>
                                            </ul>
                                        </nav>                          
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Header bottom area end -->
            </header>
            <!-- HRADER AREA END -->
           
            <master-comp></master-comp>

     
      
     
     
            
            <footer class="footer-area">
                
                
               
                <!-- footer copyright area -->
                <div class="footer-copyright-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-lg-6 col-md-6">
                                <p>Copyright © Suvro Datta. All Rights Reserved.</p>
                            </div>
                            <div class="col-sm-12 col-lg-6 col-md-6 pull-right">
                                <img src="{{ asset('users/images/icons/payment-icon.png') }}" alt="payment icon">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer copyright area end -->
            </footer>
            
        </div>
        <!-- Body main wrapper end -->


        <script src="users/js/jquery-3.2.1.min.js"></script>

        <!-- Popper min js -->
        <script src="users/js/popper.min.js"></script>
        <!-- Bootstrap min js  -->
        <script src="users/js/bootstrap.min.js"></script>
        <!-- nivo slider pack js  -->
        <script src="users/js/jquery.nivo.slider.pack.js"></script>
        <!-- All plugins here -->
        <script src="users/js/plugins.js"></script>
        <!-- Main js  -->
        <script src="users/js/main.js"></script>



        <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
        <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
        </script>
        
        <script src="js/app.js"></script>
       
    </body>

</html>
