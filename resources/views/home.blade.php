@extends('users.app')

@section('main-content')

<div>
    <!-- Slider area -->
    <div class="slider-area">
        <div class="container">
            <div class="row">

                <div class="col-xl-3 col-lg-3 hidden-md hidden-sm pull-left category-wrapper">
                  <div class="categori-menu">
                  <span class="categorie-title">Categories</span>
                  <nav>
                      <ul class="categori-menu-list menu-hidden">
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/1.png') }}" alt="menu-icon"></span>Electronics<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                              <!-- categori Mega-Menu Start -->
                              <ul class="ht-dropdown megamenu first-megamenu">
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Cameras</li>
                                          <li><a href="shop.html">Cords and Cables</a></li>
                                          <li><a href="shop.html">gps accessories</a></li>
                                          <li><a href="shop.html">Microphones</a></li>
                                          <li><a href="shop.html">Wireless Transmitters</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Digital Cameras</li>
                                          <li><a href="shop.html">Camera one</a></li>
                                          <li><a href="shop.html">Camera two</a></li>
                                          <li><a href="shop.html">Camera three</a></li>
                                          <li><a href="shop.html">Camera four</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Digital Cameras</li>
                                          <li><a href="shop.html">Camera one</a></li>
                                          <li><a href="shop.html">Camera two</a></li>
                                          <li><a href="shop.html">Camera three</a></li>
                                          <li><a href="shop.html">Camera four</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                              </ul>
                              <!-- categori Mega-Menu End -->
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/2.png') }}" alt="menu-icon"></span>Fashion<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                              <!-- categori Mega-Menu Start -->
                              <ul class="ht-dropdown megamenu megamenu-two">
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Men’s Fashion</li>
                                          <li><a href="shop.html">Blazers</a></li>
                                          <li><a href="shop.html">Boots</a></li>
                                          <li><a href="shop.html">pants</a></li>
                                          <li><a href="shop.html">Tops & Tees</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Women’s Fashion</li>
                                          <li><a href="shop.html">Bags</a></li>
                                          <li><a href="shop.html">Bottoms</a></li>
                                          <li><a href="shop.html">Shirts</a></li>
                                          <li><a href="shop.html">Tailored</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                              </ul>
                              <!-- categori Mega-Menu End -->
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/3.png') }}" alt="menu-icon"></span>Home & Kitchen<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                              <!-- categori Mega-Menu Start -->
                              <ul class="ht-dropdown megamenu megamenu-two">
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Large Appliances</li>
                                          <li><a href="shop.html">Armchairs</a></li>
                                          <li><a href="shop.html">Bunk Bed</a></li>
                                          <li><a href="shop.html">Mattress</a></li>
                                          <li><a href="shop.html">Sideboard</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Small Appliances</li>
                                          <li><a href="shop.html">Bootees Bags</a></li>
                                          <li><a href="shop.html">Jackets</a></li>
                                          <li><a href="shop.html">Shelf</a></li>
                                          <li><a href="shop.html">Shoes</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                              </ul>
                              <!-- categori Mega-Menu End -->
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/4.png') }}" alt="menu-icon"></span>Phones & Tablets<i class="fa fa-angle-right" aria-hidden="true"></i>
                                                      </a>
                              <!-- categori Mega-Menu Start -->
                              <ul class="ht-dropdown megamenu megamenu-two">
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Tablet</li>
                                          <li><a href="shop.html">tablet one</a></li>
                                          <li><a href="shop.html">tablet two</a></li>
                                          <li><a href="shop.html">tablet three</a></li>
                                          <li><a href="shop.html">tablet four</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Smartphone</li>
                                          <li><a href="shop.html">phone one</a></li>
                                          <li><a href="shop.html">phone two</a></li>
                                          <li><a href="shop.html">phone three</a></li>
                                          <li><a href="shop.html">phone four</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                              </ul>
                              <!-- categori Mega-Menu End -->
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/5.png') }}" alt="menu-icon"></span>TV & Video<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                              <!-- categori Mega-Menu Start -->
                              <ul class="ht-dropdown megamenu megamenu-two">
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Gaming Desktops</li>
                                          <li><a href="shop.html">Alpha Desktop</a></li>
                                          <li><a href="shop.html">rober Desktop</a></li>
                                          <li><a href="shop.html">Ultra Desktop </a></li>
                                          <li><a href="shop.html">beta desktop</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Women’s Fashion</li>
                                          <li><a href="shop.html">3D-Capable</a></li>
                                          <li><a href="shop.html">Clearance</a></li>
                                          <li><a href="shop.html">Free Shipping Eligible</a></li>
                                          <li><a href="shop.html">On Sale</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                              </ul>
                              <!-- categori Mega-Menu End -->
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/6.png') }}" alt="menu-icon"></span>Beauty</a>
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/7.png') }}" alt="menu-icon"></span>Sport & tourism</a>
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/8.png') }}" alt="menu-icon"></span>Fruits & Veggies</a></li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/9.png') }}" alt="menu-icon"></span>Computer & Laptop</a></li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/10.png') }}" alt="menu-icon"></span>Meat & Seafood</a></li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/12.png') }}" alt="menu-icon"></span>Samsung</a></li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/11.png') }}" alt="menu-icon"></span>Sanyo</a></li>
                      </ul>
                  </nav>
                      
                  </div>
                </div>
                <div class="slider col-xl-9">
                
                    <!-- slider-area start -->
                    <div class="slider-area-inner">
                        <!-- slider start -->
                        <div class="slider-inner">
                            <div id="mainSlider" class="nivoSlider nevo-slider">
                                <img src="{{ asset('users/images/slider/1.jpg') }}" alt="main slider" title="#htmlcaption1"/>
                                <img src="{{ asset('users/images/slider/2.jpg') }}" alt="main slider" title="#htmlcaption2"/>
                            </div>  
                            <div id="htmlcaption1" class="nivo-html-caption slider-caption">
                                <div class="slider-progress"></div>
                                <div class="col-md-9">
                                    <div class="slider-content slider-content-1 slider-animated-1">
                                        <h1 class="hone">INNOVATIVE</h1>
                                        <h1 class="htwo">SMARTER</h1>
                                        <h1 class="hthree">BRIGHTER</h1>
                                        <h3>40” SkyHi Smart Package</h3> 
                                        <div class="button-1 hover-btn-2">
                                            <a href="shop.html">SHOP NOW</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="htmlcaption2" class="nivo-html-caption slider-caption">
                                <div class="slider-progress"></div>
                                <div class="col-md-9">
                                    <div class="slider-content slider-content-2 slider-animated-2">
                                        <h1 class="hone">DRONE DIY</h1>
                                        <h1 class="htwo">WORKSHOP</h1>
                                        <h3 class="h3one">Build & Fly</h3>
                                        <h3 class="h3two">Your Own drone!</h3>
                                        <div class="button-1 hover-btn-1">
                                            <a href="shop.html">SHOP NOW</a>
                                        </div>
                                    </div>
                                </div>                  
                            </div>
                        </div>
                        <!-- slider end -->
                    </div>
                    <!-- slider-area end -->
                </div>
                <div class="slider-banner-area col-sm-12">
                    <div class="slider-banner">
                        <div class="slider-single-banner">
                            <a href="shop.html">
                                <img src="{{ asset('users/images/banner/banner1.jpg') }}" alt="Banner">
                            </a>
                        </div>
                    </div>
                    <div class="slider-banner">
                        <div class="slider-single-banner">
                            <a href="shop.html">
                                <img src="{{ asset('users/images/banner/banner2.jpg') }}" alt="Banner">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="slider-area">
        <div class="container">
            <div class="row">
            </div>
        </div>
    </div>

     <div class="home-fullwidth-banner-area">
        <div class="container">
            <div class="row">
            </div>
        </div>
    </div>
    <!-- Slider area end -->
    <!-- Policy area -->
    <div class="policy-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="policy-area-inner">
                        <div class="single-policy-area">
                            <div class="single-policy">
                                <div class="icon"><i class="icon ion-android-sync"></i></div>
                                <h3>Free Return</h3>
                                <p>30 days money back guarantee!</p>
                            </div>
                        </div>
                        <div class="single-policy-area">
                            <div class="single-policy">
                                <div class="icon"><i class="icon ion-paper-airplane"></i></div>
                                <h3>Free Shipping</h3>
                                <p>Free shipping on all order</p>
                            </div>
                        </div>
                        <div class="single-policy-area">
                            <div class="single-policy">
                                <div class="icon"><i class="icon ion-help-buoy"></i></div>
                                <h3>Support 24/7</h3>
                                <p>We support online 24 hrs a day</p>
                            </div>
                        </div>
                        <div class="single-policy-area">
                            <div class="single-policy">
                                <div class="icon"><i class="icon ion-card"></i></div>
                                <h3>Receive Gift Card</h3>
                                <p>Recieve gift all over oder $50</p>
                            </div>
                        </div>
                        <div class="single-policy-area">
                            <div class="single-policy">
                                <div class="icon"><i class="icon ion-ios-locked"></i></div>
                                <h3>Secure Payment</h3>
                                <p>We Value Your Security</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
    <!-- Policy area end -->
    <!-- Home fullwidth banner -->
    <div class="home-fullwidth-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <a href="#">
                        <img src="{{ asset('users/images/banner/home1-banner2.jpg') }}" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Home fullwidth banner end -->
    <!-- Best sellers -->
    <div class="product-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h3>Best Sellers</h3>
                    </div>
                </div>
            </div>
            <div class="product-area-inner">
                <div class="row">
                    <div class="product-carousel-active-2 owl-carousel">
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/1.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Aliquam lobortis est</a></h2>
                                            <span class="price">
                                                $ 80.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/2.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Aliquam lobortis</a></h2>
                                            <span class="price">
                                                <del>$ 150.00</del> $ 145.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/3.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Tincidunt malesuada</a></h2>
                                            <span class="price">
                                                <del>$ 80.00</del> $ 50.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/9.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                            <span class="price">
                                                <del>$ 85.00</del> $ 75.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <span class="onsale">Sale!</span>
                                                <img src="{{ asset('users/images/product/5.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Donec eu libero</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/6.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Pellentesque posuere</a></h2>
                                            <span class="price">
                                                $ 45.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/7.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Cras neque metus</a></h2>
                                            <span class="price">
                                                 $ 70.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/8.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                $ 80.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Best sellers end -->
    <!-- Home fullwidth banner -->
    <div class="home-fullwidth-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <a href="#">
                        <img src="{{ asset('users/images/banner/home1-banner3.jpg') }}" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Home fullwidth banner end -->
    <!-- Best sellers -->
    <div class="product-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h3>New Arrivals</h3>
                    </div>
                </div>
            </div>
            <div class="product-area-inner">
                <div class="row">
                    <div class="product-carousel-active-3 owl-carousel">
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/1.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/2.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Nulla sed libero</a></h2>
                                            <span class="price">
                                                $ 45.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/3.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Pellentesque posuere</a></h2>
                                            <span class="price">
                                                $ 100.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/4.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Tincidunt malesuada</a></h2>
                                            <span class="price">
                                                <del>$ 80.00</del> $ 50.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/5.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Pellentesque posuere</a></h2>
                                            <span class="price">
                                                $ 45.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/6.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Kaoreet lobortis</a></h2>
                                            <span class="price">
                                                $ 95.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/7.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Aliquam lobortis est</a></h2>
                                            <span class="price">
                                                $ 80.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <span class="onsale">Sale!</span>
                                                <img src="{{ asset('users/images/product/8.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/9.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Phasellus vel hendrerit</a></h2>
                                            <span class="price">
                                                $ 55.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/10.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                            <span class="price">
                                                <del>$ 85.00</del> $ 75.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/11.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/12.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/13.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <span class="onsale">Sale!</span>
                                                <img src="{{ asset('users/images/product/1.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="col-sm-12">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/2.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper gridview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/3.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                        </div>
                                        <div class="product-hidden">
                                            <div class="add-to-cart">
                                                <a href="cart.html">Add to cart</a>
                                            </div>
                                            <div class="star-actions">
                                                <div class="product-rattings">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star-half-o"></i></span>
                                                    <span><i class="fa fa-star-o"></i></span>
                                                </div>
                                                <ul class="actions">
                                                    <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Best sellers end -->
    <!-- home banner four -->
    <div class="home-banner-four">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 banner-four-one">
                    <a href="shop.html"><img src="{{ asset('users/images/banner/ehome1-banner4-1.jpg') }}" alt=""></a>
                </div>
                <div class="col-sm-7 banner-four-two">
                    <div class="top-banner">
                        <a href="shop.html"><img src="{{ asset('users/images/banner/home1-banner4-2.jpg') }}" alt=""></a>
                    </div>
                    <div class="bottom-banner">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="shop.html"><img src="{{ asset('users/images/banner/home1-banner4-3.jpg') }}" alt=""></a>
                            </div>
                            <div class="col-sm-6">
                                <a href="shop.html"><img src="{{ asset('users/images/banner/home1-banner4-4.jpg') }}" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- home banner four end -->
    <!-- product carosel area -->
    <div class="product-carosel-area">
        <div class="container">
            <div class="row">
                <!-- Product column --> 
                <div class="col-md-6 col-xl-3">
                    <div class="section-title">
                        <h3>TELEVISIONS</h3>
                    </div>
                    <div class="mini-product carosel-next-prive  owl-carousel">
                        <div class="mini-product-listview">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/18.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Itaque earum</a></h2>
                                            <span class="price">
                                                $ 866.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/19.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Nostrum exercitationem</a></h2>
                                            <span class="price">
                                                <del>$ 590.00</del>$ 550.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/20.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Consequuntur</a></h2>
                                            <span class="price">
                                                $ 366.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/21.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Porro quisquam</a></h2>
                                            <span class="price">
                                                $ 88.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="mini-product-listview">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/22.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/1.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/2.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/3.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="mini-product-listview">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/4.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/5.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/6.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/7.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                    </div>    
                </div>
                <!-- Product column end -->
                <!-- Product column --> 
                <div class="col-md-6 col-xl-3">
                    <div class="section-title">
                        <h3>CAMERAS</h3>
                    </div>
                    <div class="mini-product carosel-next-prive owl-carousel">
                        <div class="mini-product-listview">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/6.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Duis pulvinar</a></h2>
                                            <span class="price">
                                                $ 84.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/7.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Porro quisquam</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 68.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/8.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Laudantium</a></h2>
                                            <span class="price">
                                                $ 75.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/9.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Nullam maximus</a></h2>
                                            <span class="price">
                                                $ 95.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="mini-product-listview">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/10.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Nemo enim</a></h2>
                                            <span class="price">
                                                $ 55.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/11.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Natus erro</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/12.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Epicuri per</a></h2>
                                            <span class="price">
                                                $ 88.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/13.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="mini-product-listview">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/14.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                            <span class="price">
                                                 $ 63.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/15.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Nulla sed libero</a></h2>
                                            <span class="price">
                                                $ 86.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/16.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Pellentesque posuere</a></h2>
                                            <span class="price">
                                                $ 78.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/17.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                    </div>    
                </div>
                <!-- Product column end -->
                <!-- Product column --> 
                <div class="col-md-6 col-xl-3">
                    <div class="section-title">
                        <h3>DRONE</h3>
                    </div>
                    <div class="mini-product carosel-next-prive owl-carousel">
                        <div class="mini-product-listview">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/1.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Donec tempus</a></h2>
                                            <span class="price">
                                                $ 82.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/2.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sed tempor</a></h2>
                                            <span class="price">
                                                <del>$ 87.00</del> $ 80.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/3.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Cras neque</a></h2>
                                            <span class="price">
                                                $ 70.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/4.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Pellentesque ultricies</a></h2>
                                            <span class="price">
                                                $ 85.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="mini-product-listview">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/5.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Vestibulum suscipit</a></h2>
                                            <span class="price">
                                                $ 84.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/6.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Nemo enim</a></h2>
                                            <span class="price">
                                                $ 220.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/7.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Epicuri per</a></h2>
                                            <span class="price">
                                                $ 152.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/8.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="mini-product-listview">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/9.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Laudantium</a></h2>
                                            <span class="price">
                                                $ 86.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/10.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Porro quisquam</a></h2>
                                            <span class="price">
                                                $ 68.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/11.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/12.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                    </div>    
                </div>
                <!-- Product column end -->
                <!-- Product column --> 
                <div class="col-md-6 col-xl-3">
                    <div class="section-title">
                        <h3>CELL PHONES</h3>
                    </div>
                    <div class="mini-product carosel-next-prive owl-carousel">
                        <div class="mini-product-listview">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/13.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Vulputate justo</a></h2>
                                            <span class="price">
                                                $ 95.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/14.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Aliquam lobortis est</a></h2>
                                            <span class="price">
                                                $ 97.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/15.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Dignissim</a></h2>
                                            <span class="price">
                                                $ 82.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/16.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="mini-product-listview">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/17.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Aliquam lobortis</a></h2>
                                            <span class="price">
                                                $ 166.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/18.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Condimentum posuere</a></h2>
                                            <span class="price">
                                                $ 115.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/19.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/20.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Phasellus vel hendrerit</a></h2>
                                            <span class="price">
                                                $ 42.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                        <div class="mini-product-listview">
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/21.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                            <span class="price">
                                                $ 75.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/22.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Phasellus vel hendrerit</a></h2>
                                            <span class="price">
                                                $ 55.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/1.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            <!-- single product -->
                            <div class="single-product-area">
                                <div class="product-wrapper listview">
                                    <div class="list-col4">
                                        <div class="product-image">
                                            <a href="#">
                                                <img src="{{ asset('users/images/product/mini/2.jpg') }}" alt="">
                                            </a>
                                            <div class="quickviewbtn">
                                                <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-col8">
                                        <div class="product-info">
                                            <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <a href="cart.html">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                        </div>
                    </div>    
                </div>
                <!-- Product column end -->
            </div>
        </div>
    </div>
    <!-- product carosel area end -->
    <!-- Brand zone area -->
    <div class="brand-zone-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xl-9 hometab">
                    <div class="section-title">
                        <h3>Brand Zone</h3>
                    </div>
                    <div class="brand-zone-tab-area">
                        <div class="brand-tab-menu">
                            <div class="nav">
                                <ul>
                                    <li><a class="active" data-toggle="tab" href="#brand_zone_1"><span>Gamepad</span></a></li>
                                    <li><a data-toggle="tab" href="#brand_zone_2"><span>TVS</span></a></li>
                                    <li><a data-toggle="tab" href="#brand_zone_3"><span>Cell Phones</span></a></li>
                                    <li><a data-toggle="tab" href="#brand_zone_4"><span>Cameras</span></a></li>
                                    <li><a data-toggle="tab" href="#brand_zone_5"><span>Printers</span></a></li>
                                    <li><a data-toggle="tab" href="#brand_zone_6"><span>Audio & video</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="brand-tab-content">
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="brand_zone_1">
                                    <a href="#"><img src="{{ asset('users/images/banner/home1-tab-img.jpg') }}" alt=""></a>
                                </div>
                                <div class="tab-pane fade" id="brand_zone_2">
                                    <a href="#"><img src="{{ asset('users/images/banner/home1-tab-img.jpg') }}" alt=""></a>
                                </div>
                                <div class="tab-pane fade" id="brand_zone_3">
                                    <a href="#"><img src="{{ asset('users/images/banner/home1-tab-img.jpg') }}" alt=""></a>
                                </div>
                                <div class="tab-pane fade" id="brand_zone_4">
                                    <a href="#"><img src="{{ asset('users/images/banner/home1-tab-img.jpg') }}" alt=""></a>
                                </div>
                                <div class="tab-pane fade" id="brand_zone_5">
                                    <a href="#"><img src="{{ asset('users/images/banner/home1-tab-img.jpg') }}" alt=""></a>
                                </div>
                                <div class="tab-pane fade" id="brand_zone_6">
                                    <a href="#"><img src="{{ asset('users/images/banner/home1-tab-img.jpg') }}" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-xl-3 hometestimonial">
                    <div class="section-title">
                        <h3>Testimonials</h3>
                    </div>
                    <div class="testimonial-sidebar carosel-next-prive owl-carousel">
                        <!-- Testimonial area -->
                        <div class="testimonial-area">
                            <img src="{{ asset('users/images/testimonials/testimonial3-120x120.jpg') }}" alt="testimonial">
                            <blockquote class="testimonials-text"><p>RoadThemes support and response has been amazing, helping me with several issues I came across and got them solved almost the same day. A pleasure to work with them!</p>
                            </blockquote>
                            <span>Katherine Sullivan</span>
                        </div>
                        <!-- Testimonial area end -->
                        <!-- Testimonial area -->
                        <div class="testimonial-area">
                            <img src="{{ asset('users/images/testimonials/testimonial5-120x120.jpg') }}" alt="testimonial">
                            <blockquote class="testimonials-text"><p>RoadThemes support and response has been amazing, helping me with several issues I came across and got them solved almost the same day. A pleasure to work with them!</p>
                            </blockquote>
                            <span>Jenifer Brown</span>
                        </div>
                        <!-- Testimonial area end -->
                        <!-- Testimonial area -->
                        <div class="testimonial-area">
                            <img src="{{ asset('users/images/testimonials/testimonial6-120x120.jpg') }}" alt="testimonial">
                            <blockquote class="testimonials-text"><p>RoadThemes support and response has been amazing, helping me with several issues I came across and got them solved almost the same day. A pleasure to work with them!</p>
                            </blockquote>
                            <span>Kathy Young</span>
                        </div>
                        <!-- Testimonial area end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Brand zone area end -->
    <!-- Brand logo area -->
    <div class="brand-logo-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="brand-carousel-active owl-carousel">
                        <!-- single brand logo -->
                        <div class="brand-logo">
                            <a href="#"><img src="{{ asset('users/images/brand/brand1.jpg') }}" alt="Brand logo"></a>
                        </div>
                        <!-- single brand logo end -->
                        <!-- single brand logo -->
                        <div class="brand-logo">
                            <a href="#"><img src="{{ asset('users/images/brand/brand2.jpg') }}" alt="Brand logo"></a>
                        </div>
                        <!-- single brand logo end -->
                        <!-- single brand logo -->
                        <div class="brand-logo">
                            <a href="#"><img src="{{ asset('users/images/brand/brand3.jpg') }}" alt="Brand logo"></a>
                        </div>
                        <!-- single brand logo end -->
                        <!-- single brand logo -->
                        <div class="brand-logo">
                            <a href="#"><img src="{{ asset('users/images/brand/brand4.jpg') }}" alt="Brand logo"></a>
                        </div>
                        <!-- single brand logo end -->
                        <!-- single brand logo -->
                        <div class="brand-logo">
                            <a href="#"><img src="{{ asset('users/images/brand/brand5.jpg') }}" alt="Brand logo"></a>
                        </div>
                        <!-- single brand logo end -->
                        <!-- single brand logo -->
                        <div class="brand-logo">
                            <a href="#"><img src="{{ asset('users/images/brand/brand6.jpg') }}" alt="Brand logo"></a>
                        </div>
                        <!-- single brand logo end -->
                        <!-- single brand logo -->
                        <div class="brand-logo">
                            <a href="#"><img src="{{ asset('users/images/brand/brand7.jpg') }}" alt="Brand logo"></a>
                        </div>
                        <!-- single brand logo end -->
                        <!-- single brand logo -->
                        <div class="brand-logo">
                            <a href="#"><img src="{{ asset('users/images/brand/brand8.jpg') }}" alt="Brand logo"></a>
                        </div>
                        <!-- single brand logo end -->
                        <!-- single brand logo -->
                        <div class="brand-logo">
                            <a href="#"><img src="{{ asset('users/images/brand/brand9.jpg') }}" alt="Brand logo"></a>
                        </div>
                        <!-- single brand logo end -->
                        <!-- single brand logo -->
                        <div class="brand-logo">
                            <a href="#"><img src="{{ asset('users/images/brand/brand1.jpg') }}" alt="Brand logo"></a>
                        </div>
                        <!-- single brand logo end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Brand logo area end -->
</div>

@endsection

@section('scripts')

        <script src="users/js/jquery-3.2.1.min.js"></script>

        <!-- Popper min js -->
        <script src="users/js/popper.min.js"></script>
        <!-- Bootstrap min js  -->
        <script src="users/js/bootstrap.min.js"></script>
    <!-- nivo slider pack js  -->
        <script src="users/js/jquery.nivo.slider.pack.js"></script>
        <!-- All plugins here -->
        <script src="users/js/plugins.js"></script>
        <!-- Main js  -->
        <script src="users/js/main.js"></script>

        <script src="users/js/analytics.js" async defer></script>

@endsection

@section('csses')

<link rel="stylesheet" href="users/css/bootstrap.min.css">
<link rel="stylesheet" href="users/css/font-awesome.min.css">
<link rel="stylesheet" href="users/css/ionicons.min.css">
<link rel="stylesheet" href="users/css/css-plugins-call.css">
<link rel="stylesheet" href="users/css/bundle.css">
<link rel="stylesheet" href="users/css/main.css">
<link rel="stylesheet" href="users/css/responsive.css">
<link rel="stylesheet" href="users/css/colors.css">

@endsection