@extends('users.app')
@section('main-content')

    <!-- Breadcrumbs -->
    <div class="breadcrumbs-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="woocommerce-breadcrumb">
                        <a href="index.html">Home</a>
                        <span class="separator">/</span> Shop
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumbs End -->
    
    <!-- Shop page wraper -->
    <div class="shop-page-wraper">
        <div class="container">
            <div class="row">



                <div class="col-xs-12 col-md-3 sidebar-shop">
                <div class="sidebar-product-categori">
                         
                <div class="categori-menu">

                  <span class="categorie-title">Categories</span>
                  <nav>
                      <ul class="categori-menu-list" style="clear:both">
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/1.png') }}" alt="menu-icon"></span>Electronics<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                              <!-- categori Mega-Menu Start -->
                              <ul class="ht-dropdown megamenu first-megamenu">
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Cameras</li>
                                          <li><a href="shop.html">Cords and Cables</a></li>
                                          <li><a href="shop.html">gps accessories</a></li>
                                          <li><a href="shop.html">Microphones</a></li>
                                          <li><a href="shop.html">Wireless Transmitters</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Digital Cameras</li>
                                          <li><a href="shop.html">Camera one</a></li>
                                          <li><a href="shop.html">Camera two</a></li>
                                          <li><a href="shop.html">Camera three</a></li>
                                          <li><a href="shop.html">Camera four</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Digital Cameras</li>
                                          <li><a href="shop.html">Camera one</a></li>
                                          <li><a href="shop.html">Camera two</a></li>
                                          <li><a href="shop.html">Camera three</a></li>
                                          <li><a href="shop.html">Camera four</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                              </ul>
                              <!-- categori Mega-Menu End -->
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/2.png') }}" alt="menu-icon"></span>Fashion<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                              <!-- categori Mega-Menu Start -->
                              <ul class="ht-dropdown megamenu megamenu-two">
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Men’s Fashion</li>
                                          <li><a href="shop.html">Blazers</a></li>
                                          <li><a href="shop.html">Boots</a></li>
                                          <li><a href="shop.html">pants</a></li>
                                          <li><a href="shop.html">Tops & Tees</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Women’s Fashion</li>
                                          <li><a href="shop.html">Bags</a></li>
                                          <li><a href="shop.html">Bottoms</a></li>
                                          <li><a href="shop.html">Shirts</a></li>
                                          <li><a href="shop.html">Tailored</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                              </ul>
                              <!-- categori Mega-Menu End -->
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/3.png') }}" alt="menu-icon"></span>Home & Kitchen<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                              <!-- categori Mega-Menu Start -->
                              <ul class="ht-dropdown megamenu megamenu-two">
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Large Appliances</li>
                                          <li><a href="shop.html">Armchairs</a></li>
                                          <li><a href="shop.html">Bunk Bed</a></li>
                                          <li><a href="shop.html">Mattress</a></li>
                                          <li><a href="shop.html">Sideboard</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Small Appliances</li>
                                          <li><a href="shop.html">Bootees Bags</a></li>
                                          <li><a href="shop.html">Jackets</a></li>
                                          <li><a href="shop.html">Shelf</a></li>
                                          <li><a href="shop.html">Shoes</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                              </ul>
                              <!-- categori Mega-Menu End -->
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/4.png') }}" alt="menu-icon"></span>Phones & Tablets<i class="fa fa-angle-right" aria-hidden="true"></i>
                                                      </a>
                              <!-- categori Mega-Menu Start -->
                              <ul class="ht-dropdown megamenu megamenu-two">
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Tablet</li>
                                          <li><a href="shop.html">tablet one</a></li>
                                          <li><a href="shop.html">tablet two</a></li>
                                          <li><a href="shop.html">tablet three</a></li>
                                          <li><a href="shop.html">tablet four</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Smartphone</li>
                                          <li><a href="shop.html">phone one</a></li>
                                          <li><a href="shop.html">phone two</a></li>
                                          <li><a href="shop.html">phone three</a></li>
                                          <li><a href="shop.html">phone four</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                              </ul>
                              <!-- categori Mega-Menu End -->
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/5.png') }}" alt="menu-icon"></span>TV & Video<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                              <!-- categori Mega-Menu Start -->
                              <ul class="ht-dropdown megamenu megamenu-two">
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Gaming Desktops</li>
                                          <li><a href="shop.html">Alpha Desktop</a></li>
                                          <li><a href="shop.html">rober Desktop</a></li>
                                          <li><a href="shop.html">Ultra Desktop </a></li>
                                          <li><a href="shop.html">beta desktop</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                                  <!-- Single Column Start -->
                                  <li class="single-megamenu">
                                      <ul>
                                          <li class="menu-tile">Women’s Fashion</li>
                                          <li><a href="shop.html">3D-Capable</a></li>
                                          <li><a href="shop.html">Clearance</a></li>
                                          <li><a href="shop.html">Free Shipping Eligible</a></li>
                                          <li><a href="shop.html">On Sale</a></li>
                                      </ul>
                                  </li>
                                  <!-- Single Column End -->
                              </ul>
                              <!-- categori Mega-Menu End -->
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/6.png') }}" alt="menu-icon"></span>Beauty</a>
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/7.png') }}" alt="menu-icon"></span>Sport & tourism</a>
                          </li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/8.png') }}" alt="menu-icon"></span>Fruits & Veggies</a></li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/9.png') }}" alt="menu-icon"></span>Computer & Laptop</a></li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/10.png') }}" alt="menu-icon"></span>Meat & Seafood</a></li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/12.png') }}" alt="menu-icon"></span>Samsung</a></li>
                          <li><a href="shop.html"><span><img src="{{ asset('users/images/icons/11.png') }}" alt="menu-icon"></span>Sanyo</a></li>
                      </ul>
                  </nav>
                      
                </div>



                  <div class="product-filter mb-30" style="height:500px">
                     
                  </div>

                        <div class="product-filter mb-30">
                            <div class="widget-title">
                                <h3>Filter by price</h3>
                            </div>
                            <div class="widget-content">
                                <div id="price-range"></div>
                                <div class="price-values">
                                    <div class="price_text_btn">
                                        <span>Price:</span>
                                        <input type="text" class="price-amount">
                                    </div>
                                    <button class="button" type="submit">Filter</button>
                                </div>
                            </div>
                        </div>
                        <div class="widget-title">
                            <h3>SELECT BY COLOR</h3>
                        </div>
                        <div class="widget-content">
                            <ul class="product-categories">
                                <li class="cat-item">
                                    <a href="#">Gold</a>
                                    <span class="count">(1)</span>
                                </li>
                                <li class="cat-item">
                                    <a href="#">Green</a>
                                    <span class="count">(1)</span>
                                </li>
                                <li class="cat-item">
                                    <a href="#">White</a>
                                    <span class="count">(1)</span>
                                </li>
                            </ul>
                        </div>
                        <div class="product-filter mb-30">
                            <div class="widget-title">
                                <h3>TOP RATED PRODUCTS</h3>
                            </div>
                            <div class="widget-content">
                                <ul class="product_list_widget">
                                    <li class="widget-mini-product">
                                        <div class="product-image">
                                            <a title="Phasellus vel hendrerit" href="#">
                                                <img alt="" src="images/product/2.jpg') }}">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <a title="Phasellus vel hendrerit" href="#">
                                                <span class="product-title">Consequuntur magni</span>
                                            </a>
                                            <div class="star-rating">
                                                <div class="rating-box">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                </div>
                                            </div>
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">$</span>55.00</span>
                                        </div>
                                    </li>
                                    <li class="widget-mini-product">
                                        <div class="product-image">
                                            <a title="Phasellus vel hendrerit" href="#">
                                                <img alt="" src="images/product/3.jpg') }}">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <a title="Phasellus vel hendrerit" href="#">
                                                <span class="product-title">Aliquam lobortis</span>
                                            </a>
                                            <div class="star-rating">
                                                <div class="rating-box">
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                    <span><i class="fa fa-star"></i></span>
                                                </div>
                                            </div>
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">$</span>55.00</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="sidebar-single-banner">
                            <a href="#">
                                <img src="{{ asset('users/images/banner/shop-sidebar.jpg') }}" alt="Banner">
                            </a>
                        </div>
                        <div class="sidebar-tag">
                            <div class="widget-title">
                                <h3>PRODUCT TAGS</h3>
                            </div>
                            <div class="widget-content">
                                <div class="product-tags">
                                    <a href="#">New </a>
                                    <a href="#">brand</a>
                                    <a href="#">black</a>
                                    <a href="#">white</a>
                                    <a href="#">chire</a>
                                    <a href="#">table</a>
                                    <a href="#">Lorem</a>
                                    <a href="#">ipsum</a>
                                    <a href="#">dolor</a>
                                    <a href="#">sit</a>
                                    <a href="#">amet</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-9 shop-content">
                    <div class="shop-banner">
                        <img src="{{ asset('users/images/banner/shop-category.jpg') }}" alt="">
                    </div>
                    <div class="product-toolbar">
                        <div class="topbar-title">
                            <h1>Shop</h1>
                        </div>
                        <div class="product-toolbar-inner">
                            <div class="product-view-mode">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#grid"><i class="ion-grid"></i></a></li>
                                    <li><a data-toggle="tab" href="#list"><i class="ion-navicon"></i></a></li>
                                </ul>
                            </div>
                            <p class="woocommerce-result-count">Showing 1–12 of 42 results</p>
                            <div class="woocommerce-ordering">
                                <form method="get" class="woocommerce-ordering hidden-xs">
                                    <div class="orderby-wrapper">
                                        <label>Sort By :</label>
                                        <select class="nice-select-menu orderby">
                                            <option dara-display="Select">Default sorting</option>
                                            <option value="popularity">Sort by popularity</option>
                                            <option value="rating">Sort by average rating</option>
                                            <option value="date">Sort by newness</option>
                                            <option value="price">Sort by price: low to high</option>
                                            <option value="price-desc">Sort by price: high to low</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="shop-page-product-area tab-content">
                            <div id="grid" class="tab-pane fade in show active">
                                <div class="row">
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/1.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/2.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/3.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/13.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/5.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/6.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/7.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/8.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/9.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/10.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/11.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/12.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/6.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/7.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/8.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <img src="{{ asset('users/images/product/11.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2>
                                                        <span class="price">
                                                            <del>$ 85.00</del> $ 75.00
                                                        </span>
                                                    </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings">
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star-half-o"></i></span>
                                                                <span><i class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="list" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <span class="onsale">Sale!</span>
                                                            <img src="{{ asset('users/images/product/1.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                                        <span class="price">
                                                            <del>$ 77.00</del> $ 66.00
                                                        </span>
                                                        <div class="product-rattings">
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star-half-o"></i></span>
                                                            <span><i class="fa fa-star-o"></i></span>
                                                        </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i>Add to Wishlist</a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <span class="onsale">Sale!</span>
                                                            <img src="{{ asset('users/images/product/2.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                                        <span class="price">
                                                            <del>$ 77.00</del> $ 66.00
                                                        </span>
                                                        <div class="product-rattings">
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star-half-o"></i></span>
                                                            <span><i class="fa fa-star-o"></i></span>
                                                        </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i>Add to Wishlist</a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <span class="onsale">Sale!</span>
                                                            <img src="{{ asset('users/images/product/3.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                                        <span class="price">
                                                            <del>$ 77.00</del> $ 66.00
                                                        </span>
                                                        <div class="product-rattings">
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star-half-o"></i></span>
                                                            <span><i class="fa fa-star-o"></i></span>
                                                        </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i>Add to Wishlist</a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <span class="onsale">Sale!</span>
                                                            <img src="{{ asset('users/images/product/4.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                                        <span class="price">
                                                            <del>$ 77.00</del> $ 66.00
                                                        </span>
                                                        <div class="product-rattings">
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star-half-o"></i></span>
                                                            <span><i class="fa fa-star-o"></i></span>
                                                        </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i>Add to Wishlist</a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <span class="onsale">Sale!</span>
                                                            <img src="{{ asset('users/images/product/5.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                                        <span class="price">
                                                            <del>$ 77.00</del> $ 66.00
                                                        </span>
                                                        <div class="product-rattings">
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star-half-o"></i></span>
                                                            <span><i class="fa fa-star-o"></i></span>
                                                        </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i>Add to Wishlist</a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <span class="onsale">Sale!</span>
                                                            <img src="{{ asset('users/images/product/6.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                                        <span class="price">
                                                            <del>$ 77.00</del> $ 66.00
                                                        </span>
                                                        <div class="product-rattings">
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star-half-o"></i></span>
                                                            <span><i class="fa fa-star-o"></i></span>
                                                        </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i>Add to Wishlist</a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <span class="onsale">Sale!</span>
                                                            <img src="{{ asset('users/images/product/7.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                                        <span class="price">
                                                            <del>$ 77.00</del> $ 66.00
                                                        </span>
                                                        <div class="product-rattings">
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star-half-o"></i></span>
                                                            <span><i class="fa fa-star-o"></i></span>
                                                        </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i>Add to Wishlist</a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            <span class="onsale">Sale!</span>
                                                            <img src="{{ asset('users/images/product/8.jpg') }}" alt="">
                                                        </a>
                                                        <div class="quickviewbtn">
                                                            <a href="#" data-toggle="modal" data-target="#product_modal"  data-original-title="Quick View"><i class="ion-eye"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2>
                                                        <span class="price">
                                                            <del>$ 77.00</del> $ 66.00
                                                        </span>
                                                        <div class="product-rattings">
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star-half-o"></i></span>
                                                            <span><i class="fa fa-star-o"></i></span>
                                                        </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart">
                                                            <a href="cart.html">Add to cart</a>
                                                        </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href="#"><i class="ion-android-favorite-outline"></i>Add to Wishlist</a></li>
                                                                <li><a href="#"><i class="ion-ios-shuffle-strong"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav class="woocommerce-pagination">
                        <ul class="page-numbers">
                            <li><span aria-current="page" class="page-numbers current">1</span></li>
                            <li><a class="page-numbers" href="#">2</a></li>
                            <li><a class="page-numbers" href="#">3</a></li>
                            <li><a class="page-numbers" href="#">4</a></li>
                            <li><a class="next page-numbers" href="#">→</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div> 
    </div>
    <!-- Shop page wraper end -->
            
@endsection
